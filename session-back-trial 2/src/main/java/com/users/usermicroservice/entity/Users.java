package com.users.usermicroservice.entity;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "users")
@EqualsAndHashCode
public class Users {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "email")
  private String email;

  @Column(name = "name")
  private String name;

  @Column(name = "password")
  private String password;

  @Column(name = "image_url")
  private String imageUrl;

  @Column(name = "public_profile")
  private int publicProfile=1;

  public Users() {}

  public Users(String email, String name, String password, String imageUrl,int publicProfile) {
    this.email = email;
    this.name = name;
    this.password = password;
    this.publicProfile = publicProfile;
    this.imageUrl = imageUrl;
  }

  public Users(
      String email, String name, String password, Boolean publicProfile, String imageUrl) {}

  public int getId() {
    return id;
  }

  public String getEmail() {
    return email;
  }

  public String getName() {
    return name;
  }

  public String getPassword() {
    return password;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public int getPublicProfile() {
    return publicProfile;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  @Override
  public String toString() {
    return "Users{" +

            ", email='" + email + '\'' +
            ", name='" + name + '\'' +
            ", password='" + password + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", publicProfile=" + publicProfile +
            '}';
  }

  public void setPublicProfile(int publicProfile) {
    this.publicProfile = publicProfile;
  }
}
