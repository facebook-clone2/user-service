package com.users.usermicroservice.repository;

import com.users.usermicroservice.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Integer> {

  Optional<Users> findByEmailAndPassword(String email, String password);

//  Optional<Users> findByEmailAndType(String email, String type);

  Optional<Users> findByEmail(String email);

  List<Users> findByNameContaining(String name);

  Optional<Users> findById(Integer id);

}
