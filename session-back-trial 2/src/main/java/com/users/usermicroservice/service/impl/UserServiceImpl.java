package com.users.usermicroservice.service.impl;

import com.users.usermicroservice.model.FriendsModel;
import com.users.usermicroservice.model.UserModel;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface UserServiceImpl {
  UserModel userDetails(int userId);
  UserModel friendDetails(int friendUserId,int userId);
  List<FriendsModel> getFriendsDetails(List<Integer> friendsList);
//
  List<UserModel> getAllUser();
 Optional<UserModel> verify(String email);
//
  boolean updateProfileImage(Integer userId,String url);
//
  boolean create(UserModel userModel);
//
  boolean updateAccountPrivacy(Integer userId);
//
  boolean deleteUser(UserModel userModel);

  List<UserModel> searchByName(String name);

  ResponseEntity<String> getUserNameByUserId(Integer userId);

  int getAccountType(Integer userId);

  UserModel getUserDetails(Integer userId);

  String getNameOfUser(Integer userId);
}
