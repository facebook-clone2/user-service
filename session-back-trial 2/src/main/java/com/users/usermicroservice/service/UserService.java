package com.users.usermicroservice.service;

import com.users.usermicroservice.entity.Users;
import com.users.usermicroservice.model.FriendsModel;
import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserService implements com.users.usermicroservice.service.impl.UserServiceImpl {

  @Autowired private UserRepository userRepository;
  PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  @Autowired
  private RestTemplate restTemplate;
  @Override
  public UserModel userDetails(int id) {
    Optional<Users> users = userRepository.findById(id);
    UserModel userModel = new UserModel();
    if(users.isPresent()){
      userModel.setId(users.get().getId());
      userModel.setName(users.get().getName());
      userModel.setEmail(users.get().getEmail());
      userModel.setImageUrl(users.get().getImageUrl());
      userModel.setPublicProfile(users.get().getPublicProfile());
    }return userModel;
  }

  @Override
  public List<UserModel> getAllUser() {
    List<Users> users = userRepository.findAll();
    List<UserModel> userModels = new ArrayList<>();
    for (int i = 0; i < users.size(); i++) {
      UserModel userModel = new UserModel();
      userModel.setId(users.get(i).getId());
      userModel.setImageUrl(users.get(i).getImageUrl());
      userModel.setName(users.get(i).getName());
      userModel.setPublicProfile(users.get(i).getPublicProfile());
      userModels.add(userModel);
    }
    return userModels;
  }
  @Override
  public UserModel friendDetails(int friendUserId, int userId) {
    if(friendUserId==userId){
      return null;
    }
    List<Integer> pairs = new ArrayList<>();
    pairs.add(friendUserId);
    pairs.add(userId);
    Optional<Users> users = userRepository.findById(friendUserId);
    UserModel userModel = new UserModel();
    if (users.isPresent()) {
      Boolean status = restTemplate.postForObject("HTTP://10.65.1.112:8080/friends/areFriends", pairs, Boolean.class);
      userModel.setId(users.get().getId());
      userModel.setName(users.get().getName());
      userModel.setImageUrl(users.get().getImageUrl());
      userModel.setPublicProfile(users.get().getPublicProfile());
      userModel.setAreFriends(status);
    }
    return userModel;
  }

  @Override
  public Optional<UserModel> verify(String email) {
    Optional<Users> optionalUser = userRepository.findByEmail(email);
    Optional<UserModel> optionalUserModel = Optional.of(new UserModel());
    optionalUserModel.get().setId(optionalUser.get().getId());
    optionalUserModel.get().setEmail(optionalUser.get().getEmail());
    optionalUserModel.get().setName(optionalUser.get().getName());
    optionalUserModel.get().setImageUrl(optionalUser.get().getImageUrl());
    optionalUserModel.get().setPassword(optionalUser.get().getPassword());
    log.info("I am here");
    return optionalUserModel;
  }

  @Override
  public boolean updateProfileImage(Integer userId, String url) {
    Optional<Users> optionalUser = userRepository.findById(userId);
    System.out.println(optionalUser);
    return optionalUser
        .map(
            user -> {
              user.setImageUrl(url);
              userRepository.save(user);
              return true;
            })
        .orElse(false);
  }

  @Override
  public boolean updateAccountPrivacy(Integer userId) {
    Optional<Users> optionalUser = userRepository.findById(userId);
    return optionalUser
        .map(
            user -> {
              int x = user.getPublicProfile();
              if (x == 1) {
                user.setPublicProfile(0);
              } else {
                user.setPublicProfile(1);
              }
              userRepository.save(user);
              return true;
            })
        .orElse(false);
  }

  @Override
  public boolean create(UserModel userModel) {

    if (userRepository.findByEmail(userModel.getEmail()).isPresent()) {
      log.info("Email already exists: {}", userModel.getEmail());
      return false;
    }
    String hashedPassword = passwordEncoder.encode(userModel.getPassword());
    Users user = new Users();
    user.setEmail(userModel.getEmail());
    user.setImageUrl(userModel.getImageUrl());
    user.setName(userModel.getName());
    user.setPassword(hashedPassword);
    log.info("User is: {}", user);
    userRepository.save(user);
    return true;
  }

  public UserModel getUserDetails(Integer userId) {
      Optional<Users> users1 = userRepository.findById(userId);
      UserModel userModel = new UserModel();
      if (users1.isPresent()) {
        userModel.setName(users1.get().getName());
        userModel.setImageUrl(users1.get().getImageUrl());
        userModel.setPublicProfile(users1.get().getPublicProfile());
        userModel.setId(users1.get().getId());
      }
      log.info("{}",userModel);
    return userModel;
  }

  @Override
  public boolean deleteUser(UserModel userModel) {
    try {
      userRepository.deleteById(userModel.getId());
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public List<FriendsModel> getFriendsDetails(List<Integer> friendsList) {
    //        List<Users> userList = new ArrayList<>();
    List<FriendsModel> friendsModels = new ArrayList<>();
    for (Integer i : friendsList) {
      FriendsModel friendsModel = new FriendsModel();
      Optional<Users> user1 = userRepository.findById(i);
      if (user1.isPresent()) {
        friendsModel.setId(user1.get().getId());
        friendsModel.setName(user1.get().getName());
        friendsModel.setImageUrl(user1.get().getImageUrl());
        friendsModels.add(friendsModel);
      }
      }
    return friendsModels;
  }

  @Override
  public String getNameOfUser(Integer userId){
    Optional<Users> users = userRepository.findById(userId);
    if(users.isPresent()){
      return users.get().getName();
    }return null;
  }
  @Override
  public List<UserModel> searchByName(String name) {
    List<UserModel> userModels = new ArrayList<>();
    List<Users> users = userRepository.findByNameContaining(name);
    for (int i = 0; i < users.size(); i++) {
      UserModel userModel = new UserModel();
      userModel.setEmail(users.get(i).getEmail());
      userModel.setImageUrl(users.get(i).getImageUrl());
      userModel.setPassword(users.get(i).getPassword());
      userModel.setPublicProfile(users.get(i).getPublicProfile());
      userModel.setId(users.get(i).getId());
      userModel.setName(users.get(i).getName());
      userModels.add(userModel);
    }
    return userModels;
  }

  public ResponseEntity<String> getUserNameByUserId(Integer userId) {
    ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
    Optional<Users> user = userRepository.findById(userId);
    if (user.isPresent()) {
      response = new ResponseEntity<>(user.get().getName(), HttpStatus.OK);
      log.info("Username is: {}", response.getBody());
      return response;
    }
    response = new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    return response;
  }

  public int getAccountType(Integer userId) {
    return userRepository.findById(userId).get().getPublicProfile();
  }
}
