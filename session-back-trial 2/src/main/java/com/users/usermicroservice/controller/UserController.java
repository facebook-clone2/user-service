package com.users.usermicroservice.controller;

import com.users.usermicroservice.model.FriendsModel;
import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.service.UserService;
import com.users.usermicroservice.service.impl.UserServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util. concurrent.ConcurrentHashMap;
@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/v1.0/users")
public class UserController {
  @Autowired private UserServiceImpl userServiceImpl;
  @Autowired private RedisTemplate<String, String> redisTemplate;

  private final Map<Integer, Instant> lastRequestTimeMap = new ConcurrentHashMap<>();
  private static final Duration FIVE_MINUTES = Duration.ofMinutes(5);

 PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
  @GetMapping("/getDetails/{userId}")
  public ResponseEntity<Object> userDetails(
      @PathVariable int userId) {
    // Validate the token before processing the request
//    if (!validateToken(token)) {
//      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
//    }

    UserModel user = userServiceImpl.userDetails(userId);
    if (user != null) {
      return ResponseEntity.ok(user);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }
  @GetMapping("/getDetailsOfPerson/{friendUserId}/{userId}")
  public ResponseEntity<Object> friendDetails(
          @PathVariable int friendUserId, @PathVariable int userId) {
    // Validate the token before processing the request
    //    if (!validateToken(token)) {
    //      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
    //    }

    UserModel user = userServiceImpl.friendDetails(friendUserId, userId);
    if (user != null) {
      return ResponseEntity.ok(user);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid request!");
    }
  }
  @GetMapping("/getAllUsers")
  public ResponseEntity<List<UserModel>> getAllUser() {

    List<UserModel> user = userServiceImpl.getAllUser();
    if (user != null) {
      return ResponseEntity.ok(user);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
  }


  @PostMapping("/login")
  public ResponseEntity<Object> verify(@RequestBody UserModel userModel) {
    System.out.println("Verify called");
    Optional<UserModel> users =
        userServiceImpl.verify(userModel.getEmail());
    if (users.isPresent() && passwordEncoder.matches(userModel.getPassword(), users.get().getPassword())) {

      System.out.println("Inside Login");
      Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);
      String token = generateToken(users.get().getId(), key);
      Integer id = users.get().getId();
      System.out.println(id);
      Map<String, Object> response = new HashMap<>();
      response.put("token", token);
      response.put("id", id);

      return ResponseEntity.ok(response);
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
    }
  }

  @PostMapping("/updateProfileImage")
  public ResponseEntity<Object> updateProfileImage(
      @RequestBody UserModel userModel) {
//    // Validate the token before processing the request
//    if (!validateToken(token)) {
//      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
//    }

    boolean isSuccess = userServiceImpl.updateProfileImage(userModel.getId(),userModel.getImageUrl());
    log.info(String.valueOf(isSuccess));
    if (isSuccess) {
      return ResponseEntity.ok("Profile image updated successfully");
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  @PostMapping("/userDetailsForFriendRequests")
  public UserModel userDetails(@RequestBody Integer userId){
    return userServiceImpl.getUserDetails(userId);
  }
  @PostMapping("/signup")
  public ResponseEntity<Object> createUser(@RequestBody UserModel userModel) {
    log.info("Inside createUser, got userModel : {}", userModel);
    boolean isSuccess = userServiceImpl.create(userModel);
    if (isSuccess) {
   log.info("Success status: {}", isSuccess);
      return ResponseEntity.ok("User created successfully");
    } else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
          .body("User with this email already exists");
    }
  }

  @GetMapping("/updateAccountPrivacy/{userId}")
  public ResponseEntity<Object> updateAccountPrivacy(@PathVariable Integer userId) {
    // Validate the token before processing the request
    //    if (!validateToken(token)) {
    //      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
    //    }
    Instant lastRequestTime = lastRequestTimeMap.getOrDefault(userId, Instant.MIN);
    Instant now = Instant.now();

    // Check if the user has made a request within the last five minutes
    if (Duration.between(lastRequestTime, now).compareTo(FIVE_MINUTES) < 0) {
      return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS)
              .body("You are restricted from updating account privacy. Please try again later.");
    }
    boolean isSuccess = userServiceImpl.updateAccountPrivacy(userId);
    if (isSuccess) {
      return ResponseEntity.ok("Account Privacy Updated Successfully");
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  @DeleteMapping("/deleteUser/{userId}")
  public ResponseEntity<Object> deleteUser(
      @PathVariable int userId) {
    UserModel userModel = new UserModel();
    userModel.setId(userId);
    boolean isSuccess = userServiceImpl.deleteUser(userModel);
    if (isSuccess) {
      return ResponseEntity.ok("User deleted successfully");
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  @PostMapping("/friendsModel")
  public List<FriendsModel> friendsFinding(@RequestBody List<Integer> friendList){
    log.info("Hello");
    return userServiceImpl.getFriendsDetails(friendList);
  }

  @GetMapping("/searchByName/{name}")
  public List<UserModel> searchByName(@PathVariable String name){
    return userServiceImpl.searchByName(name);
  }

  @PostMapping("/getUserName")
  public String getNameByUserId(@RequestBody Integer userId){
    log.info("Got call to get userName by userId: {}", userId);
    return userServiceImpl.getUserNameByUserId(userId).getBody();
  }

  @PostMapping("/getAccountType")
  public Integer getAccountType(@RequestBody Integer userId){
    log.info("Got call to get account type of userId: {}", userId);
    return userServiceImpl.getAccountType(userId);
  }

  @PostMapping("/getNameByUserId")
  public String getNameOfUser(@RequestBody Integer userId){
    return userServiceImpl.getNameOfUser(userId);
  }
  private String generateToken(int id, Key key) {
    long expirationTime = 10 * 60 * 1000;
    String token =
        Jwts.builder()
            .setSubject(String.valueOf(id)) // Set email as the subject instead of username
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
            .signWith(SignatureAlgorithm.HS512, key)
            .compact();
    redisTemplate.opsForValue().set(token, String.valueOf(id), expirationTime, TimeUnit.MILLISECONDS);
    return token;
  }
  private boolean validateToken(String token) {
    String storedid = redisTemplate.opsForValue().get(token);
    return storedid == null || !storedid.equals(getUsernameFromToken(token));
  }

  private String getUsernameFromToken(String token) {
    return Jwts.parserBuilder()
        .setSigningKey(Keys.secretKeyFor(SignatureAlgorithm.HS512))
        .build()
        .parseClaimsJws(token)
        .getBody()
        .getSubject();
  }

}
