package com.users.usermicroservice.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserModel {
  private int id;

  private String email;

  private String name;

  private String password;

  private String imageUrl;

  private int publicProfile;

  private boolean areFriends;
}
